
'''
Author: Arunas Tuzikas
Description: checking is projector at smart conference room active
Date: 1/12/2017
'''

#!/usr/bin/env python

import socket
import threading
import sys
import time
import urllib2

# Global constants used by the server
TCP_IP = '192.168.0.3'
TCP_SERVER_PORT = 60001

# Create a TCP?IP socket

while 1:
  try:
    urllib2.urlopen("http://192.168.0.251", timeout=1)
    print "Projector on"
    proj_status = "PRO 1"
  except urllib2.URLError as err:
    print "Projector off"
    proj_status = "PRO 0"
  
  try:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((TCP_IP, TCP_SERVER_PORT))
    sock.send(proj_status)
    sock.close()  
  except:
    print "Server is not active"
  time.sleep(5)
  
