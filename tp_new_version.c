#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <assert.h>
#include <fcntl.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>

#include <curl/curl.h>
#define TO_HEX(i) (i <= 9 ? '0' + i : 'A' - 10 + i)
void new_window_2(GtkWidget *newWindow);
void new_window_3(GtkWidget *newWindow);
int t = 30;

gdouble CCT       = 3500;
gdouble QUALITY   = 0;
gdouble INTENSITY = 80;

GtkWidget *slid1;
GtkWidget *slid2;
GtkWidget *slid3;

GtkWidget *window;
GtkWidget *window_1;
GtkWidget *window_2;
GtkWidget *window_3;
GtkWidget *window_psw;
GtkWidget *fixed;
GtkWidget *btn1;
GtkWidget *btn2;
GtkWidget *btn3;


GtkWidget *pic1;
GtkWidget *pic2;
GtkWidget *pic3;
GtkWidget *pic4;
CURL *curl;
char CCT_hex[10] = {'C','C','T',' ','3','0','0','0','\n','\0'};
char INT_hex[9] = {'I','N','T',' ','0',' ',' ','\n','\0'};
char RES_hex[7] = {'R','E','S',' ','0','\n','\0'};
char MAN_hex[7] = {'M','A','N',' ','0','\n','\0'};
//char PRO_hex[7] = {'P','R','O',' ','0','\n','\0'};
int MAN = 0;
int RES = 0;
//int PRO = 0;
char old_pro_value='0';

//static gboolean check_proj(gpointer data)
//{
//  int sockfd = socket(AF_INET,SOCK_STREAM,0);
//  struct sockaddr_in sin;
//  sin.sin_family = AF_INET;
//  sin.sin_port   = htons(65432);  // Could be anything
//  inet_pton(AF_INET, "192.168.0.251", &sin.sin_addr);
//
//  if (connect(sockfd, (struct sockaddr *) &sin, sizeof(sin)) == -1)
//  {
    //printf("Error connecting 192.168.0.251: %d (%s)\n", errno, strerror(errno));
//    if (errno==113)
//    {
//      PRO_hex[4]='0';
//    }
//    else
//    {
//      PRO_hex[4]='1';
//    }
//  }
//  return TRUE;
//}

void send_input(void)
{
    int socket_desc, msg;
  struct sockaddr_in server;
  //char value[40];
  char value[40];
  char number[3];
  char local_base[13];
  //check_projector(curl);
  strcpy(value,CCT_hex);
  strcat(value,INT_hex);
//  strcat(value,PRO_hex);
  strcat(value,MAN_hex);
  strcat(value,RES_hex);
  socket_desc = socket(AF_INET , SOCK_STREAM , 0);

  if (socket_desc < 0) {
    printf("ERROR opening socket\n");

  }

  else {
    server.sin_addr.s_addr = inet_addr("192.168.0.3");
    server.sin_family = AF_INET;
    server.sin_port = htons( 60001 );
    //Connect to remote server
    connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
    //Send some data
    send(socket_desc, value, strlen(value), 0);
    //msg = recv(socket_desc, server_reply, 20 , 0);
    close(socket_desc);
    //printf("msg=%d, %s\n", msg, server_reply);
  }
}

//void check_projector(CURL *curl)
//{
  //CURL *curl = curl_easy_init();
//  CURLcode res;
//  if(curl)
//  {
//    curl_easy_setopt(curl,CURLOPT_URL,"http://192.168.0.251");
//    res=curl_easy_perform(curl);
//    if(res!=CURLE_OK)
//    {
      //fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
      //curl_easy_cleanup(curl);
//      PRO_hex[4]='0';
//    }
//    else
//    {
//      g_print("succuess");
      //curl_easy_cleanup(curl);
//      PRO_hex[4]='1';
//    }
//  }
//  if(PRO_hex[4]!=old_pro_value)
//    {
//      send_input();
//      old_pro_value = PRO_hex[4];
//    }
//}

void value_changed1(GtkRange *range, gpointer win) {
    int value;
    char CCT_value[4];
    CCT = gtk_range_get_value(range);
    value= CCT;
    sprintf(CCT_value,"%d",value);
    CCT_hex[4] = CCT_value[0];
    CCT_hex[5] = CCT_value[1];
    CCT_hex[6] = CCT_value[2];
    CCT_hex[7] = CCT_value[3];
    send_input();
}

void value_changed3(GtkRange *range, gpointer win) {
    
    QUALITY = gtk_range_get_value(range);
}
void value_changed2(GtkRange *range, gpointer win) {
    int value;
    char INT_value[3];
    INTENSITY  = gtk_range_get_value(range);
    value= INTENSITY;
    sprintf(INT_value,"%d",value);
    g_print("%c",INT_value[0]);
    g_print("%c",INT_value[1]);
    g_print("%c",INT_value[2]);
    if(value<10)
      INT_hex[4]=INT_value[0];
    else if(value<100)
    {
      INT_hex[4] = INT_value[0];
      INT_hex[5] = INT_value[1];
    }  
    else
    { 
      INT_hex[4] = INT_value[0];
      INT_hex[5] = INT_value[1]; 
      INT_hex[6] = INT_value[2];
    }
    send_input();
}

void close_window_1(void)
{
  gtk_widget_destroy(window_1);
  MAN_hex[4]='0';
  send_input();
}

void new_Window_1(GtkWidget *newWindow){
  gtk_window_fullscreen(GTK_WINDOW(newWindow));
  GtkWidget *pic_1_1;
  GtkWidget *pic_2_1;
  GtkWidget *pic_3_1;
  GtkWidget *pic_4_1;
  GtkWidget *slid1_1;
  GtkWidget *slid2_1;
  GtkWidget *slid3_1;
  GtkWidget *BackButton;
  GdkColor color; 
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;
  MAN_hex[4]='1';
  send_input();
  gtk_widget_modify_bg(newWindow, GTK_STATE_NORMAL, &color);
  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(newWindow), fixed);
  
  pic_1_1 = gtk_image_new_from_file ("SLb.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic_1_1, 0, 0);
  
  pic_2_1 = gtk_image_new_from_file ("CCT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic_2_1, 15, 120);
  
  pic_3_1 = gtk_image_new_from_file ("INT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic_3_1, 145, 120);
  
  pic_4_1 = gtk_image_new_from_file ("QUA.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic_4_1, 275, 120);

  slid1_1 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 2500, 5000, 100);
  gtk_fixed_put(GTK_FIXED(fixed), slid1_1, 33, 180);
  gtk_widget_set_size_request(slid1_1, 100, 350);
  
  slid2_1 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0, 99, 1);
  gtk_fixed_put(GTK_FIXED(fixed), slid2_1, 163, 180);
  gtk_widget_set_size_request(slid2_1, 100, 350);

  slid3_1 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, -100, 100, 1);
  gtk_fixed_put(GTK_FIXED(fixed), slid3_1, 293, 180);
  gtk_widget_set_size_request(slid3_1, 100, 350); 

  g_signal_connect(G_OBJECT(slid1_1), "value-changed", G_CALLBACK(value_changed1), slid1_1);  
  g_signal_connect(G_OBJECT(slid2_1), "value-changed", G_CALLBACK(value_changed2), slid2_1);  
  g_signal_connect(G_OBJECT(slid3_1), "value-changed", G_CALLBACK(value_changed3), slid3_1);  

  gtk_range_set_value(GTK_RANGE(slid1_1), 3000);
  gtk_range_set_value(GTK_RANGE(slid2_1), 0);
  gtk_range_set_value(GTK_RANGE(slid3_1), 0);

  BackButton = gtk_button_new_with_label("Back");
  gtk_fixed_put(GTK_FIXED(fixed), BackButton, 430, 450);
  gtk_widget_set_size_request(BackButton, 260, 100);
  g_signal_connect_swapped(G_OBJECT(BackButton), "clicked", G_CALLBACK(close_window_1), newWindow);
  gtk_widget_show_all(newWindow);

  
}

void button1_clicked(GtkWidget *widget, gpointer data) {  
  
  window_1 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  new_Window_1(window_1);

}

void confirm_password_1(GtkWidget *widget, gpointer userdata){
  GtkEntry *entry = userdata;
  const gchar *text = gtk_entry_get_text(entry);
  char psw[4];
  psw[0]='1';
  psw[1]='2';
  psw[2]='3';
  psw[3]='4';
  g_print("text is %s",text);
  if(text[0]==psw[0] && text[1]==psw[1] && text[2]==psw[2] && text[3]==psw[3]){
    gtk_widget_destroy(window_psw);
    window_2 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    new_window_2(window_2);
  }
  else
    gtk_widget_destroy(window_psw);
}

void confirm_password_2(GtkWidget *widget, gpointer userdata){
  GtkEntry *entry = userdata;
  const gchar *text = gtk_entry_get_text(entry);
  char psw[4];
  psw[0]='1';
  psw[1]='2';
  psw[2]='3';
  psw[3]='4';
  if(text[0]==psw[0] && text[1]==psw[1] && text[2]==psw[2] && text[3]==psw[3]){
    gtk_widget_destroy(window_psw);
    gtk_widget_destroy(window_2);
  }
  else
    gtk_widget_destroy(window_psw);
}

void confirm_password_3(GtkWidget *widget, gpointer userdata){
  GtkEntry *entry = userdata;
  const gchar *text = gtk_entry_get_text(entry);
  char psw[4];
  psw[0]='1';
  psw[1]='2';
  psw[2]='3';
  psw[3]='4';
  if(text[0]==psw[0] && text[1]==psw[1] && text[2]==psw[2] && text[3]==psw[3]){
    gtk_widget_destroy(window_psw);
    window_3 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    new_window_3(window_3);
  }
  else
    gtk_widget_destroy(window_psw);
}

void get_button_clicked(GtkWidget *widget, gpointer userdata){
  GtkEntry *entry = userdata;
  const gchar *text = gtk_button_get_label(GTK_BUTTON(widget));
  gint position = gtk_entry_get_text_length(entry);
  gtk_editable_insert_text(GTK_EDITABLE(entry),text,-1,&position);
}


void password_window(GtkWidget *newWindow, int num){
  GtkWidget *entry;
  GtkWidget *box1, *box2;
  GtkWidget *button, *key_1, *key_2, *key_3,*key_4,*key_5,*key_6,*key_7,*key_8,*key_9,*key_0;
  box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
  
  gtk_window_set_default_size(GTK_WINDOW(newWindow),500,400);
  gtk_window_set_title(GTK_WINDOW(newWindow),"PASSWORD");

  gtk_container_add(GTK_CONTAINER(newWindow),box1);
  gtk_container_add(GTK_CONTAINER(box1),box2);
  gtk_widget_show(box1);
  gtk_widget_show(box2);
  entry = gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(entry),100);

  gtk_box_pack_start(GTK_BOX(box1),entry,TRUE,TRUE,0);
  gtk_widget_show(entry);
  
  button = gtk_button_new_with_label("CONFIRM");
  if (num==1){
    g_signal_connect(button, "clicked",G_CALLBACK(confirm_password_1),entry);
  }
  else if (num==2){
    g_signal_connect(button, "clicked",G_CALLBACK(confirm_password_2),entry);
  }
  else if(num==3){
    g_signal_connect(button,"clicked",G_CALLBACK(confirm_password_3),entry);
  }
  
  gtk_box_pack_start(GTK_BOX(box1),button,TRUE,TRUE,0);
  gtk_widget_show(button);

  key_1 = gtk_button_new_with_label("1");
  key_2 = gtk_button_new_with_label("2");
  key_3 = gtk_button_new_with_label("3");
  key_4 = gtk_button_new_with_label("4");
  key_5 = gtk_button_new_with_label("5");
  key_6 = gtk_button_new_with_label("6");
  key_7 = gtk_button_new_with_label("7");
  key_8 = gtk_button_new_with_label("8");
  key_9 = gtk_button_new_with_label("9");
  key_0 = gtk_button_new_with_label("0");
  g_signal_connect(key_1, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_2, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_3, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_4, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_5, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_6, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_7, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_8, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_9, "clicked",G_CALLBACK(get_button_clicked),entry);
  g_signal_connect(key_0, "clicked",G_CALLBACK(get_button_clicked),entry);
  gtk_box_pack_start(GTK_BOX(box2),key_1,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_2,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_3,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_4,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_5,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_6,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_7,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_8,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_9,TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(box2),key_0,TRUE,TRUE,0);
  gtk_widget_show(key_1);
  gtk_widget_show(key_2);
  gtk_widget_show(key_3);
  gtk_widget_show(key_4);
  gtk_widget_show(key_5);
  gtk_widget_show(key_6);
  gtk_widget_show(key_7);
  gtk_widget_show(key_8);
  gtk_widget_show(key_9);
  gtk_widget_show(key_0);
  gtk_widget_show(newWindow);
}

void BackButton2_clicked(GtkWidget *window){
  window_psw = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window_psw),GTK_WIN_POS_CENTER);
  password_window(window_psw,2);
}

void new_window_2(GtkWidget *newWindow){
  gtk_window_fullscreen(GTK_WINDOW(newWindow));
  GdkColor color; 
  GtkWidget *label;
  GtkWidget *BackButton;
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;
  gtk_widget_modify_bg(newWindow, GTK_STATE_NORMAL, &color);
  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(newWindow), fixed);
  
  gchar *str = "<b>Experiment is running</b>";

  label = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(label), str);

  
  
  gtk_fixed_put(GTK_FIXED(fixed),label,200,200);
  
  BackButton = gtk_button_new_with_label("Back");
  gtk_fixed_put(GTK_FIXED(fixed), BackButton, 430, 450);
  gtk_widget_set_size_request(BackButton, 260, 100);
  g_signal_connect(G_OBJECT(BackButton), "clicked", G_CALLBACK(BackButton2_clicked), newWindow);
  gtk_widget_show_all(newWindow);
}

void button2_clicked(GtkWidget *widget) {
  window_psw = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window_psw),GTK_WIN_POS_CENTER);
  password_window(window_psw,1);
  //window_2 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  //new_window_2(window_2);
  
}

/*
gboolean close_window(gpointer window){
    gtk_widget_destroy(GTK_WIDGET(window));
    return FALSE;
}
*/
static gboolean
_label_update(gpointer data)
{
    GtkLabel *label = (GtkLabel*)data;
    char buf[256];
    memset(&buf, 0x0, 256);
    snprintf(buf, 255, "Waiting for %d secs", t--);
    gtk_label_set_label(label, buf);
    //gchar *str2 = "<b>Waiting for 30 sec...</b>";
    //label_2 = gtk_label_new(NULL);
    //gtk_label_set_markup(GTK_LABEL(label_2), str2);
    //gtk_fixed_put(GTK_FIXED(fixed),label_2,200,400);
    if (t==0){
      gtk_widget_destroy(window_3);
      RES_hex[4]='0';
      send_input();
      t=30;
      return FALSE;
      }
    else
      return TRUE;
}


void new_window_3(GtkWidget *newWindow){
  gtk_window_fullscreen(GTK_WINDOW(newWindow));
  GdkColor color; 
  GtkWidget *label_1, *label_2;
  GtkWidget *BackButton;
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;
  RES_hex[4]='1';
  send_input();
  gtk_widget_modify_bg(newWindow, GTK_STATE_NORMAL, &color);
  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(newWindow), fixed);
  gchar *str1 = "<b>System is restarting</b>";
  label_1 = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(label_1), str1);
  gtk_fixed_put(GTK_FIXED(fixed),label_1,200,200);
  //gchar *str2 = "<b>Waiting for 30 sec...</b>";
  label_2 = gtk_label_new(NULL);
  //gtk_label_set_markup(GTK_LABEL(label_2), str2);
  gtk_fixed_put(GTK_FIXED(fixed),label_2,200,400);
  g_timeout_add_seconds(1, _label_update, label_2);

  gtk_widget_show_all(newWindow);
  //g_timeout_add_seconds(30, (GSourceFunc)close_window, newWindow);
}

void button3_clicked(GtkWidget *widget, gpointer data) {  
  //GtkWidget *newWindow;
  //window_3 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  //new_window_3(window_3);
  window_psw = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window_psw),GTK_WIN_POS_CENTER);
  password_window(window_psw,3);
}


int main(int argc, char *argv[])
{
  curl = curl_easy_init();
  GdkColor color; 
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;
  gtk_init(&argc, &argv);
  FILE *file;
  int i, k,j;
//  j = check_proj();
//  g_print("%d\n",j);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_fullscreen(GTK_WINDOW(window));
  gtk_widget_modify_bg(window, GTK_STATE_NORMAL, &color);

  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(window), fixed);
  
  pic1 = gtk_image_new_from_file ("SLb.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic1, 0, 0);
  
  pic2 = gtk_image_new_from_file ("CCT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic2, 15, 120);
  
  pic3 = gtk_image_new_from_file ("INT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic3, 180, 120);
  
  //pic4 = gtk_image_new_from_file ("QUA.png");
  //gtk_fixed_put(GTK_FIXED(fixed), pic4, 275, 120);
  slid1 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 2500, 5000, 100);
  gtk_fixed_put(GTK_FIXED(fixed), slid1, 33, 180);
  gtk_widget_set_size_request(slid1, 100, 350);
  
  slid2 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0, 99, 1);
  gtk_fixed_put(GTK_FIXED(fixed), slid2, 203, 180);
  gtk_widget_set_size_request(slid2, 100, 350);
  
  btn1 = gtk_button_new_with_label("GO TO FULL CONTROL");
  gtk_fixed_put(GTK_FIXED(fixed), btn1, 430, 150);
  gtk_widget_set_size_request(btn1, 260, 100);

  btn2 = gtk_button_new_with_label("Experiment In Progess");
  gtk_fixed_put(GTK_FIXED(fixed), btn2, 430, 300);
  gtk_widget_set_size_request(btn2, 260, 100);

  btn3 = gtk_button_new_with_label("Restart");
  gtk_fixed_put(GTK_FIXED(fixed), btn3, 430, 450);
  gtk_widget_set_size_request(btn3, 260, 100);

  

  gtk_widget_show_all(window);
  
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);  

  g_signal_connect(G_OBJECT(btn1), "clicked", G_CALLBACK(button1_clicked), NULL);
  g_signal_connect(G_OBJECT(btn2), "clicked", G_CALLBACK(button2_clicked), NULL);
  g_signal_connect(G_OBJECT(btn3), "clicked", G_CALLBACK(button3_clicked), NULL);
  gtk_range_set_increments(GTK_RANGE(slid1),500,500);
  gtk_range_set_increments(GTK_RANGE(slid2),10,1);
  g_signal_connect(G_OBJECT(slid1), "value-changed", G_CALLBACK(value_changed1), slid1);  
  g_signal_connect(G_OBJECT(slid2), "value-changed", G_CALLBACK(value_changed2), slid2);
  
  gtk_range_set_value(GTK_RANGE(slid1), 3000);
  gtk_range_set_value(GTK_RANGE(slid2), 0);
  
  CCT       = 3000;
  QUALITY   = 0;
  INTENSITY = 0;

  gtk_main();
//g_timeout_add_seconds(10,check_proj,NULL);

  return 0;
}
