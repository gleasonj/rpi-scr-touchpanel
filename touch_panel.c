#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <assert.h>
#include <fcntl.h>


#define TO_HEX(i) (i <= 9 ? '0' + i : 'A' - 10 + i)

gdouble CCT       = 3500;
gdouble QUALITY   = 0;
gdouble INTENSITY = 80;


  GtkWidget *slid1;
  GtkWidget *slid2;
  GtkWidget *slid3;

float CONST[26][15];
float LINEAR[5][2];
float send_blue, send_green, send_amber, send_red, send_white;

char BASE[13] = "192.168.0.";
int IPs[10];

int TotalNumber = 10;

int DONE = 0;



volatile uint32_t *mvpwmregs = 0;


void FindIPs(void){
  char number[3];
  char server_reply[7];
  int socket_desc, IP, i, msg;
  struct sockaddr_in server;
  char local_base[13];
  struct timeval timeout;      
    timeout.tv_sec =  0;
    timeout.tv_usec = 999;
  
   
  for (i=100; i<200; i++){
    if (socket_desc < 0)
       printf("ERROR opening socket\n");
	else {
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	bzero(local_base, 13);
	bzero(server_reply, 7);
	snprintf(number, 10, "%d", i);
	strcat(local_base, BASE);
	strcat(local_base, number);
	
	// Set timeouts
	setsockopt (socket_desc, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (socket_desc, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (socket_desc, SOL_SOCKET, SO_KEEPALIVE, (char *)&timeout, sizeof(timeout));
	
	server.sin_addr.s_addr = inet_addr(local_base);
	server.sin_family = AF_INET;
	server.sin_port = htons( 57007 );
	
	//Connect to remote server
	connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
	//Send some data
	
	send(socket_desc, "PS00000000000000000000", 22, 0);
	msg = recv(socket_desc, server_reply , 7 , 0);
	printf("IP=%s, msg = %d, %s\n", local_base, msg, server_reply);
	if (msg == 7) {
		
	//	printf("IP=%s, msg = %d, %s\n", local_base, msg, server_reply);
		IPs[TotalNumber] = i;
		TotalNumber++;
	}
	close(socket_desc);
	}
  }
  for(i=0; i<TotalNumber; i++)
	printf("IP=192.168.0.%d\n", IPs[i]);
	

}


void set_brightness(int pct) {
	if(mvpwmregs == 0) {
		int mem = open("/dev/mem", O_RDWR|O_SYNC);
		assert(mem != -1);
		mvpwmregs = (unsigned int *) mmap(0, 4096,
	  	  PROT_READ | PROT_WRITE, MAP_SHARED, mem, 0xd401a000);
	}
 
	// duty cycle floor is 57, max is 128
	mvpwmregs[0x004/4] = 57 + ((pct * 71)/100);
}


void SendRaw(int ch, int blu, int gre, int amb, int red, int whi){
  char *message, server_reply[7], RAW[22], status[22];
  char blu_hex[4], gre_hex[4], amb_hex[4], red_hex[4], whi_hex[4];
  int socket_desc, msg;
  struct sockaddr_in server;
  char number[3];
  char local_base[13];
  
  struct timeval timeout;      
    timeout.tv_sec =  0;
    timeout.tv_usec = 100;
  
  bzero(RAW, 22);
  
  blu_hex[0] = TO_HEX(((blu & 0xF000) >> 12));   
  blu_hex[1] = TO_HEX(((blu & 0x0F00) >> 8));
  blu_hex[2] = TO_HEX(((blu & 0x00F0) >> 4));
  blu_hex[3] = TO_HEX((blu & 0x000F));
   
  gre_hex[0] = TO_HEX(((gre & 0xF000) >> 12));   
  gre_hex[1] = TO_HEX(((gre & 0x0F00) >> 8));
  gre_hex[2] = TO_HEX(((gre & 0x00F0) >> 4));
  gre_hex[3] = TO_HEX((gre & 0x000F));
   
  amb_hex[0] = TO_HEX(((amb & 0xF000) >> 12));   
  amb_hex[1] = TO_HEX(((amb & 0x0F00) >> 8));
  amb_hex[2] = TO_HEX(((amb & 0x00F0) >> 4));
  amb_hex[3] = TO_HEX((amb & 0x000F));
   
  red_hex[0] = TO_HEX(((red & 0xF000) >> 12));   
  red_hex[1] = TO_HEX(((red & 0x0F00) >> 8));
  red_hex[2] = TO_HEX(((red & 0x00F0) >> 4));
  red_hex[3] = TO_HEX((red & 0x000F));
   
  whi_hex[0] = TO_HEX(((whi & 0xF000) >> 12));   
  whi_hex[1] = TO_HEX(((whi & 0x0F00) >> 8));
  whi_hex[2] = TO_HEX(((whi & 0x00F0) >> 4));
  whi_hex[3] = TO_HEX((whi & 0x000F));
 
  RAW[0] = 'P';
  RAW[1] = 'S';
  RAW[2] = blu_hex[0];
  RAW[3] = blu_hex[1];
  RAW[4] = blu_hex[2];
  RAW[5] = blu_hex[3];
  RAW[6] = gre_hex[0];
  RAW[7] = gre_hex[1];
  RAW[8] = gre_hex[2];
  RAW[9] = gre_hex[3];
  RAW[10] = amb_hex[0];
  RAW[11] = amb_hex[1];
  RAW[12] = amb_hex[2];
  RAW[13] = amb_hex[3];
  RAW[14] = red_hex[0];
  RAW[15] = red_hex[1];
  RAW[16] = red_hex[2];
  RAW[17] = red_hex[3];
  RAW[18] = whi_hex[0];
  RAW[19] = whi_hex[1];
  RAW[20] = whi_hex[2];
  RAW[21] = whi_hex[3];
   
  message = RAW;
  status[0] = 48 + ch -111;
  status[1] = RAW[1];
  status[2] = RAW[2];
  status[3] = RAW[3];
  status[4] = RAW[4];
  status[5] = RAW[5];
  status[6] = RAW[6];
  status[7] = RAW[7];
  status[8] = RAW[8];
  status[9] = RAW[9];
  status[10] = RAW[10];
  status[11] = RAW[11];
  status[12] = RAW[12];
  status[13] = RAW[13];
  status[14] = RAW[14];
  status[15] = RAW[15];
  status[16] = RAW[16];
  status[17] = RAW[17];
  status[18] = RAW[18];
  status[19] = RAW[19];
  status[20] = RAW[20];
  status[21] = RAW[21];

 // printf("Eilute yra %s\n", status);

  SendStatus(status);
  
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc < 0)
        printf("ERROR opening socket\n");
	else {
		
		bzero(local_base, 13);
		bzero(server_reply, 7);
		snprintf(number, 10, "%d", ch);
		strcat(local_base, BASE);
		strcat(local_base, number);
		
		// Set timeouts
		setsockopt (socket_desc, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
		setsockopt (socket_desc, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
		setsockopt (socket_desc, SOL_SOCKET, SO_KEEPALIVE, (char *)&timeout, sizeof(timeout));
				
		server.sin_addr.s_addr = inet_addr(local_base);
		server.sin_family = AF_INET;
		server.sin_port = htons( 57007 );
		
		//Connect to remote server
		connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
		//Send some data
		send(socket_desc, message, strlen(message), 0);
		msg = recv(socket_desc, server_reply , 7 , 0);
		sleep(0.5);
		printf("msg=%d, server_reply=%s, chanel = %d\n", msg, server_reply, ch);
		if (msg != 7)
		{	
			send(socket_desc, message, strlen(message), 0);
			msg = recv(socket_desc, server_reply , 7 , 0);
			sleep(0.5);
			printf("Bando uzdaryti porta");
			close(socket_desc);
		}
		else
		{
			close(socket_desc);
		}
	}
}

void Update(int ch) {
    int k;
	float intens, intens_r, intens_a, intens_g, intens_b, intens_w;
	
	k=(int)((CCT - 2500)/100);
	
	if (QUALITY < 0){
		send_blue  = (CONST[k][8] - CONST[k][3]) * (1 + QUALITY/100) + CONST[k][3];
		send_green = (CONST[k][7] - CONST[k][2]) * (1 + QUALITY/100) + CONST[k][2];
		send_amber = (CONST[k][6] - CONST[k][1]) * (1 + QUALITY/100) + CONST[k][1];
		send_red   = (CONST[k][5] - CONST[k][0]) * (1 + QUALITY/100) + CONST[k][0];
		send_white = (CONST[k][9] - CONST[k][4]) * (1 + QUALITY/100) + CONST[k][4];
	}
	else {
		send_blue  = (CONST[k][13] - CONST[k][8]) * QUALITY/100 + CONST[k][8];
		send_green = (CONST[k][12] - CONST[k][7]) * QUALITY/100 + CONST[k][7];
		send_amber = (CONST[k][11] - CONST[k][6]) * QUALITY/100 + CONST[k][6];
		send_red   = (CONST[k][10] - CONST[k][5]) * QUALITY/100 + CONST[k][5];
		send_white = (CONST[k][14] - CONST[k][9]) * QUALITY/100 + CONST[k][9];
	}
	
	intens = INTENSITY/100;
	intens_b = LINEAR[0][0]*intens*intens + LINEAR[0][1]*intens; 
	intens_g = LINEAR[1][0]*intens*intens + LINEAR[1][1]*intens; 
	intens_a = LINEAR[2][0]*intens*intens + LINEAR[2][1]*intens; 
	intens_r = LINEAR[3][0]*intens*intens + LINEAR[3][1]*intens;
	intens_w = LINEAR[4][0]*intens*intens + LINEAR[4][1]*intens; 
	
	//printf("b1 = %0.5f, b2 = %0.5f, g1 = %0.5f, g2 = %0.5f, w1 = %0.5f, w2 = %0.5f\n", LINEAR[0][0], LINEAR[0][1], LINEAR[1][0], LINEAR[1][1], LINEAR[4][0], LINEAR[4][1]);
	//printf("intens = %0.5f, intensB = %0.5f, intensG = %0.5f, intensA = %0.5f, intensR = %0.5f, intensW = %0.5f\n", intens, intens_b, intens_g, intens_a, intens_r, intens_w);
	
	SendRaw(ch, (int)(65535*send_blue*intens_b), (int)(65535*send_green*intens_g),(int)(65535*send_amber*intens_a),(int)(65535*send_red*intens_r),(int)(65535*send_white*intens_w));
}

void button1_clicked(GtkWidget *widget, gpointer data) {  
  int i;
  gtk_range_set_value(slid1, 3000);
  gtk_range_set_value(slid2, 0);
  gtk_range_set_value(slid3, 50);
  CCT       = 3000;
  QUALITY   = 0;
  INTENSITY = 50;
  
  for(i=0; i<TotalNumber; i++)
	Update(IPs[i]);

  
}

void button2_clicked(GtkWidget *widget, gpointer data) {
  int i;
  gtk_range_set_value(slid1, 3000);
  gtk_range_set_value(slid2, 0);
  gtk_range_set_value(slid3, 0);
  
  CCT       = 3000;
  QUALITY   = 0;
  INTENSITY = 0;
  
  for(i=0; i<TotalNumber; i++)
	Update(IPs[i]);

  
}

void button3_clicked(GtkWidget *widget, gpointer data) {
 // int i, b, g, r, a, w;
 // for(i=0; i<TotalNumber; i++){
//	b = (int)60000*(sin(i*0.3+2.0));
//	g = (int)60000*(sin(i*0.3+0.5));
//	r = (int)60000*(sin(i*0.3+5.0));
//	a = (int)60000*(sin(i*0.3+6.5));
//	w = 0;//(int)60000*(sin(i*0.628+6.25));
//	if (b < 0) 
//	  b = 0;
//	if (g < 0)
 //     g = 0;
//	if (r < 0) 
//	  r = 0;
//	if (a < 0) 
//	  a = 0;
//	if (w < 0) 
//	  w = 0;
//	printf("Sviestuvo nr = %d, blue = %d, green = %d, amber = %d, red = %d, white = %d\n", IPs[i], b, g, a, r, w);
//	SendRaw(IPs[i], b, g, a, r, w);
	
	SendRaw(111,     0,     0,     0, 50000,     0);
	printf("Blue = 00000, green = 00000, amber = 00000, red = 50000, white = 00000\n");
	SendRaw(112,     0,     0, 40000, 10000,     0);
	printf("Blue = 00000, green = 00000, amber = 50000, red = 00000, white = 0\n");
	SendRaw(113,     0,     0, 30000, 30000,     0);
	printf("Blue = 00000, green = 00000, amber = 30000, red = 30000, white = 0\n");
	SendRaw(114,     0, 30000, 40000, 10000,     0);
	printf("Blue = 00000, green = 30000, amber = 40000, red = 10000, white = 0\n");
	SendRaw(115,     0, 40000,     0, 20000,     0);
	printf("Blue = 00000, green = 40000, amber = 00000, red = 20000, white = 0\n");
	SendRaw(116,     0, 50000,     0,     0,     0);
	printf("Blue = 00000, green = 50000, amber = 00000, red = 00000, white = 0\n");
	SendRaw(117, 50000, 20000,     0, 10000,     0);
	printf("Blue = 50000, green = 20000, amber = 00000, red = 10000, white = 0\n");
	SendRaw(118, 50000,     0,     0,     0,     0);
	printf("Blue = 50000, green = 00000, amber = 00000, red = 00000, white = 0\n");
	SendRaw(119, 50000,     0,     0, 50000,     0);
	printf("Blue = 50000, green = 00000, amber = 0, red = 50000, white = 0\n");
	SendRaw(120, 50000,     0, 50000,     0,     0);
	printf("Blue = 50000, green = 00000, amber = 50000, red = 00000, white = 0\n");

	
//  }
}

void button4_clicked(GtkWidget *widget, gpointer data){

  int i;
  for(i=0; i<TotalNumber; i++)
	Update(IPs[i]);
}

void close_window(GtkWidget *widget, gpointer window){
    gtk_main_quit();
    gtk_widget_destroy(GTK_WIDGET(window));
}

/*
Opens up a new window to display the waiting message
Takes as input a window and modifies it
*/
void new_window(GtkWidget *newWindow){

  gint x = 100;
  gint y = 100;
  //GtkWidget *newWindow;
  GtkWidget *closeButton;
  GtkWidget *fixed;
  GtkWidget *text;
  GtkTextBuffer *buffer;
  GtkWidget *pic1;
  PangoFontDescription *font_desc;

  // Initialize color, size, and title
  gtk_window_set_title(GTK_WINDOW (newWindow), "Waitscreen");
  gtk_window_fullscreen(GTK_WINDOW(newWindow));
  GdkColor color;
  color.red = 0xffff;
  color.green = 0xffff;
  color.blue = 0xffff;

  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(newWindow), fixed);

  pic1 = gtk_image_new_from_file ("SLb.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic1, 0, 0);

  // Text
  text = gtk_text_view_new();
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (text));
  gtk_text_buffer_set_text(buffer, "Demo is running", -1);
  font_desc = pango_font_description_from_string ("Serif 15");
  gtk_widget_modify_font (text, font_desc);
  pango_font_description_free (font_desc);
  gtk_fixed_put(GTK_FIXED(fixed), text, x + 250, y + 200); 

  gtk_widget_modify_bg(newWindow, GTK_STATE_NORMAL, &color);

  gtk_widget_show_all(newWindow);
}

/*
Makes a call to the DynamicDemo server to run the demo
*/
void call_demo_server()
{
  char *message, server_reply[20];
  int socket_desc, msg;
  struct sockaddr_in server;
  char number[3];
  char local_base[13];
  message = "RunDynamic";

  socket_desc = socket(AF_INET , SOCK_STREAM , 0);

  if (socket_desc < 0) {
    printf("ERROR opening socket\n");

  }

  else {
    server.sin_addr.s_addr = inet_addr("192.168.0.2");
    server.sin_family = AF_INET;
    server.sin_port = htons( 50001 );
    //Connect to remote server
    connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
    //Send some data
    send(socket_desc, message, strlen(message), 0);
    msg = recv(socket_desc, server_reply, 20 , 0);
    close(socket_desc);
    printf("msg=%d, %s\n", msg, server_reply);
  }   
}

/*
Send status information to server
*/
void SendStatus(char value[23])
{
  int socket_desc, msg;
  struct sockaddr_in server;
  char number[3];
  char local_base[13];

  socket_desc = socket(AF_INET , SOCK_STREAM , 0);

  if (socket_desc < 0) {
    printf("ERROR opening socket\n");

  }

  else {
    server.sin_addr.s_addr = inet_addr("192.168.0.2");
    server.sin_family = AF_INET;
    server.sin_port = htons( 50003 );
    //Connect to remote server
    connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
    //Send some data
    send(socket_desc, value, strlen(value), 0);
    //msg = recv(socket_desc, server_reply, 20 , 0);
    close(socket_desc);
    //printf("msg=%d, %s\n", msg, server_reply);
  }   
}



/*
Runs a thread to call to the DynamicDemo server, 
When call is complete, it closes the waiting window and nested loop.
*/
void call_server_thread(GtkWidget *window){
  //gdk_threads_enter();
  printf("In thread\n");
  call_demo_server();

  printf("About to kill window\n");
  
  gtk_widget_destroy(GTK_WIDGET(window));
  //gdk_threads_leave();
  printf("Exiting thread\n");
}

/*
  Starts the DynamicDemo and display a wait screen.
  To have the wait screen appear as the demo is running, the call to the 
  DynamicDemo server is made in a sperate thread. When the demo is finished, the 
  new window is then destroyed, and the user is taken back to the home screen. 
*/
void button5_clicked(GtkWidget *widget) {
  gpointer window = NULL;
  char *message, server_reply[7];
  int socket_desc, msg;
  struct sockaddr_in server;
  char number[3];
  char local_base[13];
  GThread *thread;
  GError *error = NULL;
  GtkWidget *newWindow;

  // Initialize then display window
  newWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  new_window(newWindow);

  //Debug statements  
  printf("Clicked button 5\n");
  printf("Initialized threads\n");

  //Initialize threading
  g_type_init();

  if( ! g_thread_supported() ){
      g_thread_init( NULL );
  }

  gdk_threads_init();
  
  //Debug statements
  sleep(0.5);
  printf("Done initializing threads\n");
  
  //launch the thread to call the the server
  thread = g_thread_create(call_server_thread, newWindow, FALSE, &error );

  // Debug statements
  printf("Outta here\n");

}

/*
Calls raritan power server
Arguments:
cmd: String to be sent to the server, in the form
     "Command,x,y" 
     x = ports to be changed (ex. 12 for ports 1 and 2)
     y = action, 0 to turn off, 1 to turn on
*/
void call_raritan_server(char *command){
  char *message, server_reply[20];
  int socket_desc, msg;
  struct sockaddr_in server;
  char number[3];
  char local_base[13];
  message = command;

  socket_desc = socket(AF_INET , SOCK_STREAM , 0);

  if (socket_desc < 0) {
    printf("ERROR opening socket\n");

  }

  else {
    server.sin_addr.s_addr = inet_addr("192.168.0.2");
    server.sin_family = AF_INET;
    server.sin_port = htons( 50002 );
    //Connect to remote server
    connect(socket_desc, (struct sockaddr *)&server, sizeof(server));
    //Send some data
    send(socket_desc, message, strlen(message), 0);
    msg = recv(socket_desc, server_reply, 20 , 0);
    close(socket_desc);
    printf("msg=%d, %s\n", msg, server_reply);
  }   
}

/*
  When pressed, button will close window and turn off raritan power unit
*/
void close_raritan_button(GtkWidget *widget, GtkWidget *window){
  //Change 4 to 12 to control ports 1 and 2
  call_raritan_server("Control,56,0");
  gtk_widget_destroy(GTK_WIDGET(window));

}

/*
Opens up a new window to indicate the the VLC demo is running
*/
void new_window_for_raritan(GtkWidget *newWindow){

  gint x = 100;
  gint y = 100;

  GtkWidget *closeButton;
  GtkWidget *fixed;
  GtkWidget *text;
  GtkTextBuffer *buffer;
  GtkWidget *pic1;
  PangoFontDescription *font_desc;

  // Initialize color, size, and title
  gtk_window_set_title(GTK_WINDOW (newWindow), "Waitscreen");
  gtk_window_fullscreen(GTK_WINDOW(newWindow));

  GdkColor color;
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;

  gtk_widget_modify_bg(newWindow, GTK_STATE_NORMAL, &color);

  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(newWindow), fixed);

  // Add LESA logo and button
  pic1 = gtk_image_new_from_file ("SLb.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic1, 0, 0);

  closeButton= gtk_button_new_with_label("<< STOP DEMO >>");
  gtk_fixed_put(GTK_FIXED(fixed), closeButton, 350, 300);
  gtk_widget_set_size_request(closeButton, 160, 70);
  g_signal_connect(G_OBJECT(closeButton), "clicked", G_CALLBACK(close_raritan_button), newWindow);

  gtk_widget_show_all(newWindow);
}

/*
  Turn on raritan power supply and open up the waiting window
  NOTE: currently turns on/off port 5,6
*/
void button6_clicked(GtkWidget *widget){

  GtkWidget *newWindow;
  //Currently changes port 4, can change to 12 to control
  //ports 1 and 2
  call_raritan_server("Control,56,1");
  newWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  new_window_for_raritan(newWindow);
}

void value_changed1(GtkRange *range, gpointer win) {
    CCT = gtk_range_get_value(range);	
}

void value_changed2(GtkRange *range, gpointer win) {
    
    QUALITY = gtk_range_get_value(range);
}
void value_changed3(GtkRange *range, gpointer win) {
 	
    INTENSITY  = gtk_range_get_value(range);	
}

int main(int argc, char *argv[]) {
    
  set_brightness(99);
  
  GdkColor color; 
  color.red   = 0xFFFF;
  color.green = 0xFFFF;
  color.blue  = 0xFFFF;
  
  GtkWidget *window;
  GtkWidget *fixed;
  GtkWidget *btn1;
  GtkWidget *btn2;
  GtkWidget *btn3;
  GtkWidget *btn4;
  GtkWidget *btn5;
  GtkWidget *btn6;
  GtkWidget *btn7;
  GtkWidget *btn8;

  GtkWidget *pic1;
  GtkWidget *pic2;
  GtkWidget *pic3;
  GtkWidget *pic4;
  
  gtk_init(&argc, &argv);
  
  FILE *file;
  int i, k;
  IPs[0] = 111;
  IPs[1] = 112;
  IPs[2] = 113;
  IPs[3] = 114;
  IPs[4] = 115;
  IPs[5] = 116;
  IPs[6] = 117;
  IPs[7] = 118;
  IPs[8] = 119;
  IPs[9] = 120;
  
  file = fopen("LED_data_CCT.txt", "r");
  for (i = 0; i < 26; i++) {
        for (k = 0; k < 15; k++)
			fscanf(file, "%f,", &CONST[i][k] );
  }
  fclose(file);
    
  file = fopen("LED_data_int.txt", "r");
  for (i = 0; i < 5; i++) {
        for (k = 0; k < 2; k++)
			fscanf(file, "%f,", &LINEAR[i][k] );
  }
  fclose(file);
  
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_fullscreen(GTK_WINDOW(window));
  gtk_widget_modify_bg(window, GTK_STATE_NORMAL, &color);

  fixed = gtk_fixed_new();
  gtk_container_add(GTK_CONTAINER(window), fixed);
  
  pic1 = gtk_image_new_from_file ("SLb.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic1, 0, 0);
  
  pic2 = gtk_image_new_from_file ("CCT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic2, 15, 120);
  
  pic3 = gtk_image_new_from_file ("QUA.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic3, 145, 120);
  
  pic4 = gtk_image_new_from_file ("INT.png");
  gtk_fixed_put(GTK_FIXED(fixed), pic4, 275, 120);
   

  btn1 = gtk_button_new_with_label("<< ON >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn1, 430, 150);
  gtk_widget_set_size_request(btn1, 160, 70);

  btn2 = gtk_button_new_with_label("<< OFF >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn2, 430, 250);
  gtk_widget_set_size_request(btn2, 160, 70);

  btn3 = gtk_button_new_with_label("<< GRADIENT >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn3, 430, 350);
  gtk_widget_set_size_request(btn3, 160, 70);
  
  btn4 = gtk_button_new_with_label("<< SET VALUE >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn4, 430, 450);
  gtk_widget_set_size_request(btn4, 160, 70);
  
  btn5 = gtk_button_new_with_label("<< DEMO 1 >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn5, 610, 150);
  gtk_widget_set_size_request(btn5, 160, 70);
  
  btn6 = gtk_button_new_with_label("<< VLC DEMO >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn6, 610, 250);
  gtk_widget_set_size_request(btn6, 160, 70);
  
  btn7 = gtk_button_new_with_label("<<        >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn7, 610, 350);
  gtk_widget_set_size_request(btn7, 160, 70);
  
  btn8 = gtk_button_new_with_label("<<        >>");
  gtk_fixed_put(GTK_FIXED(fixed), btn8, 610, 450);
  gtk_widget_set_size_request(btn8, 160, 70);
  
  
  slid1 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 2500, 5000, 100);
  gtk_fixed_put(GTK_FIXED(fixed), slid1, 33, 180);
  gtk_widget_set_size_request(slid1, 100, 350);
  
  slid2 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, -100, 100, 1);
  gtk_fixed_put(GTK_FIXED(fixed), slid2, 163, 180);
  gtk_widget_set_size_request(slid2, 100, 350);
  
  slid3 = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0, 99, 1);
  gtk_fixed_put(GTK_FIXED(fixed), slid3, 293, 180);
  gtk_widget_set_size_request(slid3, 100, 350);
  
  gtk_widget_show_all(window);
  
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);  
  
  g_signal_connect(G_OBJECT(btn1), "clicked", G_CALLBACK(button1_clicked), NULL);
  g_signal_connect(G_OBJECT(btn2), "clicked", G_CALLBACK(button2_clicked), NULL);
  g_signal_connect(G_OBJECT(btn3), "clicked", G_CALLBACK(button3_clicked), NULL);
  g_signal_connect(G_OBJECT(btn4), "clicked", G_CALLBACK(button4_clicked), NULL);
  g_signal_connect(G_OBJECT(btn5), "clicked", G_CALLBACK(button5_clicked), NULL);
  g_signal_connect(G_OBJECT(btn6), "clicked", G_CALLBACK(button6_clicked), NULL);
  g_signal_connect(G_OBJECT(slid1), "value-changed", G_CALLBACK(value_changed1), slid1);  
  g_signal_connect(G_OBJECT(slid2), "value-changed", G_CALLBACK(value_changed2), slid2);  
  g_signal_connect(G_OBJECT(slid3), "value-changed", G_CALLBACK(value_changed3), slid3);  
  
  gtk_range_set_value(slid1, 3000);
  gtk_range_set_value(slid2, 0);
  gtk_range_set_value(slid3, 0);
  
  CCT       = 3000;
  QUALITY   = 0;
  INTENSITY = 0;
  
  gtk_main();


  return 0;
}
